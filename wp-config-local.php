<?php
$table_prefix  = 'prefix_';

define('DB_NAME', 'myapp');
define('DB_USER', 'dev');
define('DB_PASSWORD', 'dev');
define('DB_HOST', 'db');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

define('WP_HOME',    'http://local.example.com');
define('WP_SITEURL', 'http://local.example.com');

define('WPLANG', 'cs_CZ');
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);
define('WP_CACHE', false);
define('WPCACHEHOME ', false);
define('DISALLOW_FILE_EDIT', true);
