#!/usr/bin/env php
<?php

$json   = file_get_contents( 'config.json' );
$config = json_decode( $json );
$basePath = __DIR__ . '/' .$config->dirname;

//create folder
mkdir($config->dirname, 0777);
chdir($config->dirname);
mkdir( 'tmp', 0777 );
mkdir( 'logs', 0777 );
mkdir( 'root', 0777 );
mkdir( 'sessions', 0777 );

//docker install
system( 'git clone git@bitbucket.org:wrongwarecore/docker.git' );

//klonovani projektu z gitu
chdir( $basePath . '/root' );
system( 'git clone ' . $config->git_clone . ' .');

//database download
chdir( $basePath . '/docker' );
shell_exec( 'curl '. $config->sql_dump .' | tar xvj' );

//copy wp config
chdir( __DIR__ );
copy('wp-config-local.php', $config->dirname . '/root/wp-config-local.php');

//change hosts file
$Winpath = ( 'C:\Windows\system32\drivers\etc\hosts' );
$MACpath = ( '/private/etc/hosts' );
$Linuxpath = ( '/etc/hosts' );
$searchString = $config->url;

if(PHP_OS == 'WINNT') {
	$wincontent = file_get_contents($Winpath);
			//kontrola existence stringu v hosts
			if(strpos($wincontent, $searchString) == FALSE) {
				$open_hosts_win=fopen($Winpath,"a+");
				fwrite($open_hosts_win, "\n127.0.0.1	" . $config->url);
			fclose($open_hosts_win);
		}
	//turn off mysql and apache
	shell_exec('taskkill -f -im httpd.exe -T');
	shell_exec('taskkill -f -im mysqld.exe -T');
}

if(PHP_OS == 'Darwin' ) {
	$MACcontent = file_get_contents($MACpath);
			//kontrola existence stringu v hosts
			if(strpos($MACcontent, $searchString) == FALSE) {
			$open_hosts_mac=fopen($MACpath, 'a+' );
			fwrite($open_hosts_mac, "\n127.0.0.1	" . $config->url);
		fclose($open_hosts_mac);
	}
	//turn off mysql and apache
	shell_exec('sudo launchctl unload /Library/LaunchDaemons/mysql.agent.plist');
	shell_exec('sudo apachectl stop');
	//flush DNS
	shell_exec('sudo dscacheutil -flushcache');
	shell_exec('sudo killall -HUP mDNSResponder');
}

if(PHP_OS == 'Linux' ) {
	$Linuxcontent = file_get_contents($Linuxpath);
			//kontrola existence stringu v hosts
			if(strpos($Linuxcontent, $searchString) == FALSE) {
			$open_hosts_Linux=fopen($Linuxpath, 'a+' );
			fwrite($open_hosts_Linux, "\n127.0.0.1 " .$config->url);
		fclose($open_hosts_Linux);
	}
	//turn off mysql and apache
	shell_exec('sudo service mysql stop');
	shell_exec('sudo service apache2 stop');
}

chdir( __DIR__ );

//chown targeting all files in all folders to change rights from root to the user
function recurse_chown_chgrp($mypath, $uid, $gid) {
	$d = opendir ($mypath) ;
		while(($file = readdir($d)) !== false) {
			if ($file != "." && $file != "..") {

				$typepath = $mypath . "/" . $file ;

				//print $typepath. " : " . filetype ($typepath). "<BR>" ;
				if (filetype ($typepath) == 'dir') {
						recurse_chown_chgrp ($typepath, $uid, $gid);
				}

			chown($typepath, $uid);
			chgrp($typepath, $gid);

		}
	}
}

recurse_chown_chgrp (getcwd(), get_current_user(), getmygid()) ;

//run docker
chdir($basePath . '/docker');
shell_exec('php prepare.php');
sleep(10);
shell_exec('php build.php');
sleep(10);
shell_exec('php run.php');
sleep(10);
shell_exec('php import.php');

echo 'Done!';
